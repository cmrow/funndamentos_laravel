<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_movie', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->BigInteger('status_id')->unsigned();
            $table->BigInteger('category_id')->unsigned();
            $table->BigInteger('movie_id')->unsigned();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade');
            $table->foreign('movie_id')->references(('id'))->on('movies')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_movie');
    }
}
