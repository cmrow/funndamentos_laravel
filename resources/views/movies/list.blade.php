@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row">
        <article class="col-md-12">
            <form action="{{route('movie/show')}}" method="POST" novalidate class="form-inline">
                <div class="form-group ">
                    <label>Nombre</label>
                    <input type="text" class="form-control ml-1" name="name">
                </div>
                <div class="form-control ml-1" style="height: 50px">
                    <button type="submit" class="btn btn-default">buscar</button>
                    <a href="{{route('movie.index')}}" class="btn btn-primary">Todo</a>
                    <a href="{{route('movie.create')}}" class="btn btn-primary">Crear</a>
                </div>
                @csrf
            </form>
        </article>
        <article class="col-md-12 mt-1">
            <table class=" text-center table table-condenced table-striped table-bordered">
                <thead>
                    <tr class="">
                        <th>Nombre</th>
                        <th>Descrioción</th>
                        <th style="width: 250px">Acción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($movies as $movie)
                    <tr>
                        <td>{{$movie->name}}</td>
                        <td>{{$movie->description}}</td>
                        <td>
                            <a class="btn btn-primary btn-xs" href="{{route('movie.edit', ['id' =>$movie->id])}}">Editar</a>
                            <a class="btn btn-danger btn-xs" href="{{route('movie/destroy', ['id' =>$movie->id])}}">Eliminar</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </article>
    </div>
</section>
@endsection
