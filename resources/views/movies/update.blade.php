@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row">
        <article class="col-md-10 col-md-offset-1">
            <form action="{{route('movie.update',['id' => $movie->id])}}" method="PUT" novalidate>
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="name" class="form-control" value="{{$movie->name}}" required>
                </div>
                <div class="form-group">
                    <label for="">Descripción</label>
                    <input type="text" name="description" class="form-control" value="{{$movie->description}}" required>
                </div>
                <div class="form-control" style="border: none">
                    <button type="submit" class="btn btn-success">Enviar</button>
                </div>
                @csrf
            </form>
        </article>
    </div>

</section>
@endsection
